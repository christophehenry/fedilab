package app.fedilab.android.drawers;
/* Copyright 2019 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityOptionsCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import app.fedilab.android.R;
import app.fedilab.android.activities.BasePixelfedComposeActivity;
import app.fedilab.android.activities.SlideMediaActivity;
import app.fedilab.android.asynctasks.UpdateDescriptionAttachmentAsyncTask;
import app.fedilab.android.client.Entities.Attachment;
import app.fedilab.android.client.Entities.Error;
import app.fedilab.android.client.Entities.Status;
import app.fedilab.android.client.PixelfedAPI;
import app.fedilab.android.helper.Helper;
import app.fedilab.android.interfaces.OnRetrieveAttachmentInterface;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static android.content.Context.MODE_PRIVATE;


public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> implements OnRetrieveAttachmentInterface {

    private final ArrayList<Attachment> attachments;
    private Status status;
    private final WeakReference<Activity> contextWeakReference;
    private final boolean canDelete;
    private final SliderAdapter sliderAdapter;
    private boolean isStory = false;
    private String userId;

    public SliderAdapter(WeakReference<Activity> contextWeakReference, boolean delete, ArrayList<Attachment> attachments, String userId) {
        this.attachments = attachments;
        this.contextWeakReference = contextWeakReference;
        this.canDelete = delete;
        this.sliderAdapter = this;
        this.isStory = true;
        this.userId = userId;
    }

    public SliderAdapter(WeakReference<Activity> contextWeakReference, boolean delete, ArrayList<Attachment> attachments) {
        this.attachments = attachments;
        this.contextWeakReference = contextWeakReference;
        this.canDelete = delete;
        this.sliderAdapter = this;
    }

    public SliderAdapter(WeakReference<Activity> contextWeakReference, boolean delete, Status status) {
        this.status = status;
        this.contextWeakReference = contextWeakReference;
        this.canDelete = delete;
        this.sliderAdapter = this;
        this.attachments = status.getMedia_attachments();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, parent, false);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {

        SharedPreferences sharedpreferences = contextWeakReference.get().getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        boolean isSensitive = false;
        boolean expand_media = sharedpreferences.getBoolean(Helper.SET_EXPAND_MEDIA, false);
        if (status != null) {
            isSensitive = status.isSensitive();
        }
        if (attachments.size() > 1) {
            viewHolder.textViewDescription.setText(String.format("%s/%s", (position + 1), attachments.size()));
        }

        if (canDelete) {
            viewHolder.delete_media.setVisibility(View.VISIBLE);
        } else {
            viewHolder.delete_media.setVisibility(View.GONE);
        }
        String url;
        if (attachments.get(position).getPreview_url().endsWith("no-preview.png")) {
            url = attachments.get(position).getUrl();
        } else {
            url = attachments.get(position).getPreview_url();
        }
        if (expand_media || !isSensitive || (status != null && status.isAttachmentShown())) {
            Glide.with(viewHolder.imageViewBackground.getContext())
                    .load(url)
                    .thumbnail(0.1f)
                    .into(viewHolder.imageViewBackground);
        } else {
            Glide.with(viewHolder.imageViewBackground.getContext())
                    .load(url)
                    .thumbnail(0.1f)
                    .apply(new RequestOptions().transform(new BlurTransformation(50, 3)))
                    .into(viewHolder.imageViewBackground);
        }
        viewHolder.imageViewBackground.setContentDescription(attachments.get(position).getDescription());
        if (!this.canDelete) {
            boolean finalIsSensitive = isSensitive;
            viewHolder.imageViewBackground.setOnClickListener(v -> {
                if (status == null || expand_media || !finalIsSensitive || status.isAttachmentShown()) {
                    Intent intent = new Intent(contextWeakReference.get(), SlideMediaActivity.class);
                    Bundle b = new Bundle();

                    intent.putParcelableArrayListExtra("mediaArray", attachments);
                    b.putInt("position", (position + 1));
                    b.putInt("bgcolor", contextWeakReference.get().getResources().getColor(R.color.cyanea_primary_dark));
                    intent.putExtras(b);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptionsCompat options = ActivityOptionsCompat
                                .makeSceneTransitionAnimation(contextWeakReference.get(), viewHolder.imageViewBackground, attachments.get((position)).getUrl());
                        // start the new activity
                        contextWeakReference.get().startActivity(intent, options.toBundle());
                    } else {
                        // start the new activity
                        contextWeakReference.get().startActivity(intent);
                    }
                } else {
                    status.setAttachmentShown(true);
                    notifyDataSetChanged();
                    final int timeout = sharedpreferences.getInt(Helper.SET_NSFW_TIMEOUT, 5);
                    if (timeout > 0) {
                        new CountDownTimer((timeout * 1000), 1000) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                status.setAttachmentShown(false);
                                notifyDataSetChanged();
                            }
                        }.start();
                    }
                }
            });
        } else {
            if (!isStory) {
                viewHolder.imageViewBackground.setOnClickListener(v -> showAddDescription(attachments.get(position)));
            }
            viewHolder.delete_media.setOnClickListener(view -> showRemove(position));
        }

    }


    private void showAddDescription(final Attachment attachment) {
        SharedPreferences sharedpreferences = contextWeakReference.get().getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        int theme = sharedpreferences.getInt(Helper.SET_THEME, Helper.THEME_DARK);
        int style;
        if (theme == Helper.THEME_DARK) {
            style = R.style.DialogDark;
        } else if (theme == Helper.THEME_BLACK) {
            style = R.style.DialogBlack;
        } else {
            style = R.style.Dialog;
        }


        AlertDialog.Builder builderInner = new AlertDialog.Builder(contextWeakReference.get(), style);
        builderInner.setTitle(R.string.upload_form_description);

        View popup_media_description = contextWeakReference.get().getLayoutInflater().inflate(R.layout.popup_media_description, new LinearLayout(contextWeakReference.get()), false);
        builderInner.setView(popup_media_description);

        //Text for report
        final EditText input = popup_media_description.findViewById(R.id.media_description);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1500)});
        final ImageView media_picture = popup_media_description.findViewById(R.id.media_picture);
        Glide.with(contextWeakReference.get())
                .asBitmap()
                .load(attachment.getUrl())
                .into(new CustomTarget<Bitmap>() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> transition) {
                        media_picture.setImageBitmap(resource);
                        media_picture.setImageAlpha(60);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });

        builderInner.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());

        if (attachment.getDescription() != null && !attachment.getDescription().equals("null")) {
            input.setText(attachment.getDescription());
            input.setSelection(input.getText().length());
        }
        builderInner.setPositiveButton(R.string.validate, (dialog, which) -> {
            new UpdateDescriptionAttachmentAsyncTask(contextWeakReference.get(), attachment.getId(), input.getText().toString(), null, SliderAdapter.this);
            attachment.setDescription(input.getText().toString());
            dialog.dismiss();
        });
        AlertDialog alertDialog = builderInner.create();
        alertDialog.show();
    }


    /**
     * Removes a media
     *
     * @param position int
     */
    private void showRemove(final int position) {

        SharedPreferences sharedpreferences = contextWeakReference.get().getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        int theme = sharedpreferences.getInt(Helper.SET_THEME, Helper.THEME_DARK);
        int style;
        if (theme == Helper.THEME_DARK) {
            style = R.style.DialogDark;
        } else if (theme == Helper.THEME_BLACK) {
            style = R.style.DialogBlack;
        } else {
            style = R.style.Dialog;
        }
        AlertDialog.Builder dialog = new AlertDialog.Builder(contextWeakReference.get(), style);

        dialog.setMessage(R.string.toot_delete_media);
        dialog.setNegativeButton(R.string.cancel, (dialog1, which) -> dialog1.dismiss());
        dialog.setPositiveButton(R.string.yes, (dialog12, which) -> {
            String userIdOwner = sharedpreferences.getString(Helper.PREF_KEY_ID, null);
            if (attachments.size() > position) {
                Attachment attachment = attachments.get(position);
                if (isStory && userIdOwner != null && userId.compareTo(userIdOwner) == 0 && attachments.size() > position) {
                    new Thread(() -> new PixelfedAPI(contextWeakReference.get()).deleteStory(attachment.getId())).start();
                }
                attachments.remove(attachment);
                sliderAdapter.notifyDataSetChanged();
                if (contextWeakReference.get() instanceof BasePixelfedComposeActivity) {
                    ((BasePixelfedComposeActivity) contextWeakReference.get()).redraw();
                }
            }
            dialog12.dismiss();
        });
        dialog.show();
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return attachments.size();
    }

    @Override
    public void onRetrieveAttachment(Attachment attachment, String fileName, Error error) {

    }

    @Override
    public void onUpdateProgress(int progress) {

    }

    static class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        ImageView imageViewBackground, delete_media;
        TextView textViewDescription;

        SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            delete_media = itemView.findViewById(R.id.delete_media);
        }
    }
}