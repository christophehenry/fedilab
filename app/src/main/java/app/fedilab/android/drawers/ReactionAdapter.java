package app.fedilab.android.drawers;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import app.fedilab.android.R;
import app.fedilab.android.asynctasks.PostActionAsyncTask;
import app.fedilab.android.asynctasks.RetrieveFeedsAsyncTask;
import app.fedilab.android.client.API;
import app.fedilab.android.client.Entities.Error;
import app.fedilab.android.client.Entities.Reaction;
import app.fedilab.android.helper.Helper;
import app.fedilab.android.interfaces.OnPostActionInterface;


/**
 * Created by Thomas on 10/03/2020.
 * Adapter for reactions on messages
 */
public class ReactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnPostActionInterface {

    private List<Reaction> reactions;
    private final RetrieveFeedsAsyncTask.Type type;
    private final String statusId;

    ReactionAdapter(List<Reaction> reactions, RetrieveFeedsAsyncTask.Type type, String statusId) {
        this.reactions = reactions;
        if (reactions == null) {
            this.reactions = new ArrayList<>();
        }
        this.type = type;
        this.statusId = statusId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(layoutInflater.inflate(R.layout.drawer_reaction, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final Reaction reaction = reactions.get(viewHolder.getAdapterPosition());
        ViewHolder holder = (ViewHolder) viewHolder;
        Context context = viewHolder.itemView.getContext();
        holder.reaction_count.setText(String.valueOf(reaction.getCount()));
        if (reaction.isMe()) {
            holder.reaction_container.setBackgroundResource(R.drawable.reaction_voted);
        } else {
            holder.reaction_container.setBackgroundResource(R.drawable.reaction_border);
        }
        if (reaction.getUrl() != null) {
            holder.reaction_name.setVisibility(View.GONE);
            holder.reaction_emoji.setVisibility(View.VISIBLE);
            holder.reaction_emoji.setContentDescription(reaction.getName());
            Helper.loadGiF(holder.itemView.getContext(), reaction.getUrl(), holder.reaction_emoji);

            if (!reaction.getUrl().contains("gif")) {
                Glide.with(holder.itemView.getContext())
                        .asDrawable()
                        .load(reaction.getUrl())
                        .thumbnail(0.1f)
                        .into(holder.reaction_emoji);
            } else {
                Glide.with(holder.itemView.getContext())
                        .asGif()
                        .load(reaction.getUrl())
                        .thumbnail(0.1f)
                        .into(holder.reaction_emoji);
            }
        } else {
            holder.reaction_name.setText(reaction.getName());
            holder.reaction_name.setVisibility(View.VISIBLE);
            holder.reaction_emoji.setVisibility(View.GONE);
        }

        holder.reaction_container.setOnClickListener(v -> {
            String emojiStr = reaction.getName();
            API.StatusAction statusAction;
            if (type == RetrieveFeedsAsyncTask.Type.ANNOUNCEMENTS) {
                statusAction = reaction.isMe() ? API.StatusAction.REMOVE_REACTION : API.StatusAction.ADD_REACTION;
            } else {
                statusAction = reaction.isMe() ? API.StatusAction.REMOVE_PLEROMA_REACTION : API.StatusAction.ADD_PLEROMA_REACTION;
            }
            reaction.setMe(!reaction.isMe());
            if (reaction.isMe()) {
                reaction.setCount(reaction.getCount() + 1);
            } else {
                reaction.setCount(reaction.getCount() - 1);
            }
            new PostActionAsyncTask(context, statusAction, this.statusId, null, emojiStr, ReactionAdapter.this);
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return reactions.size();
    }

    @Override
    public void onPostAction(int statusCode, API.StatusAction statusAction, String userId, Error error) {
        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView reaction_name, reaction_count;
        LinearLayout reaction_container;
        ImageView reaction_emoji;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            reaction_name = itemView.findViewById(R.id.reaction_name);
            reaction_count = itemView.findViewById(R.id.reaction_count);
            reaction_container = itemView.findViewById(R.id.reaction_container);
            reaction_emoji = itemView.findViewById(R.id.reaction_emoji);
        }
    }


}