package app.fedilab.android.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import org.unifiedpush.android.connector.Registration;

import java.util.List;

import app.fedilab.android.R;
import app.fedilab.android.client.Entities.Account;
import app.fedilab.android.jobs.ApplicationJob;
import app.fedilab.android.jobs.NotificationsSyncJob;
import app.fedilab.android.sqlite.AccountDAO;
import app.fedilab.android.sqlite.Sqlite;

import static android.content.Context.MODE_PRIVATE;
import static app.fedilab.android.helper.BaseHelper.NOTIF_NONE;
import static app.fedilab.android.helper.BaseHelper.NOTIF_PUSH;
import static app.fedilab.android.helper.BaseHelper.liveNotifType;

public class PushHelper {

    public static void startStreaming(Context context) {
        int liveNotifications = liveNotifType(context);
        ApplicationJob.cancelAllJob(NotificationsSyncJob.NOTIFICATION_REFRESH);
        NotificationsSyncJob.schedule(false);
        switch (liveNotifications) {
            case NOTIF_PUSH:
                new Thread(() -> {
                    SQLiteDatabase db = Sqlite.getInstance(context.getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                    List<Account> accounts = new AccountDAO(context, db).getPushNotificationAccounts();
                    ((Activity) context).runOnUiThread(() -> {
                        for (Account account : accounts) {
                            registerAppWithDialog(context, account.getUsername() + "@" + account.getInstance());
                        }
                    });
                }).start();
                break;
            case NOTIF_NONE:
                new Registration().unregisterApp(context);
                break;
        }
    }


    private static void registerAppWithDialog(Context context, String slug) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        int theme = sharedpreferences.getInt(Helper.SET_THEME, Helper.THEME_DARK);
        int style;
        if (theme == Helper.THEME_DARK) {
            style = R.style.DialogDark;
        } else if (theme == Helper.THEME_BLACK) {
            style = R.style.DialogBlack;
        } else {
            style = R.style.Dialog;
        }

        Registration registration = new Registration();
        List<String> distributors = registration.getDistributors(context);
        if (distributors.size() == 1 || !registration.getDistributor(context).isEmpty()) {
            if (distributors.size() == 1) {
                registration.saveDistributor(context, distributors.get(0));
            } else {
                registration.saveDistributor(context, registration.getDistributor(context));
            }
            registration.registerApp(context, slug);
            return;
        }
        AlertDialog.Builder alert = new AlertDialog.Builder(context, style);
        if (distributors.size() == 0) {
            alert.setTitle(R.string.no_distributors_found);
            final TextView message = new TextView(context);
            String link = "https://fedilab.app/wiki/features/push-notifications/";
            final SpannableString s =
                    new SpannableString(context.getString(R.string.no_distributors_explanation, link));
            Linkify.addLinks(s, Linkify.WEB_URLS);
            message.setText(s);
            message.setPadding(30, 20, 30, 10);
            message.setMovementMethod(LinkMovementMethod.getInstance());
            alert.setView(message);
            alert.setPositiveButton(R.string.close, (dialog, whichButton) -> dialog.dismiss());
        } else {
            alert.setTitle(R.string.select_distributors);
            String[] distributorsStr = distributors.toArray(new String[0]);
            alert.setSingleChoiceItems(distributorsStr, -1, (dialog, item) -> {
                String distributor = distributorsStr[item];
                registration.saveDistributor(context, distributor);
                registration.registerApp(context, slug);
                dialog.dismiss();
            });
        }
        alert.show();
    }
}
