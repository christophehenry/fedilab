/* Copyright 2018 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import app.fedilab.android.client.API;
import app.fedilab.android.client.Entities.TrunkAccount;
import app.fedilab.android.interfaces.OnRetrieveWhoToFollowInterface;


/**
 * Created by Thomas on 10/09/2018.
 * Retrieves who to follow list
 */

public class WhoToFollowAsyncTask {

    private final String name;
    private final OnRetrieveWhoToFollowInterface listener;
    private final WeakReference<Context> contextReference;
    private List<String> response;

    public WhoToFollowAsyncTask(Context context, String name, OnRetrieveWhoToFollowInterface onRetrieveWhoToFollowInterface) {
        this.contextReference = new WeakReference<>(context);
        this.name = name;
        this.listener = onRetrieveWhoToFollowInterface;
        doInBackground();
    }

    protected void doInBackground() {
        new Thread(() -> {
            API api = new API(this.contextReference.get());
            if (name != null)
                response = api.getCommunitywikiList(name);
            else
                response = api.getCommunitywikiList();
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> {
                if (name == null)
                    listener.onRetrieveWhoToFollowList(response);
                else {
                    List<TrunkAccount> trunkAccounts = null;
                    if (response != null) {
                        trunkAccounts = new ArrayList<>();
                        for (String res : response) {
                            TrunkAccount trunkAccount = new TrunkAccount();
                            trunkAccount.setAcct(res);
                            trunkAccounts.add(trunkAccount);
                        }
                    }
                    listener.onRetrieveWhoToFollowAccount(trunkAccounts);
                }
            };
            mainHandler.post(myRunnable);
        }).start();
    }

}
